;; ================================
;; ARQUIVO DE CONFIGURAÇÃO DO EMACS
;; ================================


;; ========================
;; Gerenciamento de Pacotes
;; ========================

;; Habilita o suporte básico a pacotes
(require 'package)
;; Desativa inicialização automática do gerenciador de pacotes
(setq package-enable-at-startup nil)
;; Adiciona o site Melpa aos repositórios de pacotes disponíveis
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
;; Agora sim, inicializa o gerenciador de pacotes
(package-initialize)

;; Instala o pacote "use-package" para facilitar a instalação de outros pacotes
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))


;; ====================
;; Configurações Gerais
;; ====================


;; Desativa a tela de abertura do emacs
(setq inhibit-splash-screen t)

;; Altura da fonte. Uma fonte com height 100 terá 10pt de altura.
(set-face-attribute 'default nil :height 140)
;; Esconder barra de ferramentas
(tool-bar-mode -1)
;; Descativar beep (no PC) e vibração (no tablet/celular)
(setq visible-bell 1)
;; Lembrar dos arquivos recentes
(recentf-mode 1)

;; Fim de linha tipo unix (\n) ao invés de tipo windows (\r\n)
(setq-default buffer-file-coding-system 'utf-8-unix)
;; Utilizar caracteres tipo UNICODE (UTF8) em toda parte
(set-terminal-coding-system 'utf-8)
(set-language-environment 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)

;; Cria uma pasta para os autosaves
(make-directory "~/.emacs.d/autosaves/" t)
;; Coloca arquivos de autosave (#abc#) em ~/.emacs.d/
(setq auto-save-file-name-transforms (quote ((".*" "~/.emacs.d/autosaves/" t))))
;; Coloca arquivos de backup (abc~) em ~/.emacs.d/backups
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))

(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "C-+") 'enlarge-window)
(global-set-key (kbd "C-*") 'enlarge-window-horizontally)


;; =========================
;; Configurações dos Pacotes
;; =========================

;; Palavras-chave mais importante do "use-package":
;;   :ensure t   Instala o pacote, se ele ainda não estiver instalado.
;;   :defer t    Carrega o pacote apenas na hora em que ele for necessário.
;;   :init       Executa ações antes do pacote ser carregado.
;;   :config     Executa ações depois do pacote ser carregado.

;; OBS: load-theme precisa de t para evitar confirmação a cada início do emacs

(use-package material-theme
  :ensure t
  :config (load-theme 'material t))

;; (use-package danneskjold-theme
;;   :ensure t)

(use-package smart-mode-line
  :ensure t
  :init
  (setq sml/no-confirm-load-theme t)
  :config
  (sml/setup))
;;  (sml/apply-theme 'automatic))

(use-package auto-complete
  :ensure t
  :init
  (ac-config-default)
  (global-auto-complete-mode t))

;; Sistema de organização e planejamento "org-mode" (www.orgmode.org)
(use-package org
  :bind (("C-c l" . org-store-link)
         ("C-c c" . org-capture)
         ("C-c a" . org-agenda))
  :config
  (setq org-log-done t)  ;; Insere data/hora quando a tarefa estiver DONE
  (global-visual-line-mode t)  ;; Quebra de linhas (word wrap) no org-mode
  ;; (setq org-directory "d:/Sync/Verdesaine/Codigo-fonte/org-mode/")
  ;; Localização do arquivo de notas capturadas
  ;; (setq org-default-notes-file (concat org-directory "/notas.org"))
  (require 'org-mouse))


;; Edição de arquivos de contabilidade do ledger (www.ledger-cli.org)
;; (use-package ledger-mode
;;   :ensure t
;;   :defer t
;;   :config
;;   (setq ledger-binary-path "c:\\opt\\ledger.exe")
;; )

;; (use-package neotree
;;   :ensure t
;;   :bind (("C-\\" . 'neotree-toggle)))

(use-package pyvenv
  :ensure t
  :after (python)
  :config (pyvenv-mode))
  
;; Exibe uma lista com todas as opções, quando procurando buffers ou arquivos
(use-package ivy
  :ensure t
  :config
  (ivy-mode t)
  ;;  (setq ivy-use-virtual-buffers t)    ;; Adiciona arquivos recentes e Bookmarks
  (setq ivy-count-format "%d/%d "))    ;; Exibe número atual / número total

(use-package swiper
  :ensure t)

(use-package kivy-mode
  :ensure t
  :defer t)


;; ==================
;; ATALHOS DO VS CODE
;; ==================

(cua-mode t)  ;; C-z Desfazer, C-x Recortar, C-c Copiar, C-v Colar

(global-set-key (kbd "C-s") 'save-buffer)
;; (global-set-key (kbd "C-f") 'isearch-forward)
(global-set-key (kbd "C-f") 'swiper)
(global-set-key (kbd "C-S-k") 'kill-whole-line)

(defun vscode-previous-window()
  (interactive)
  (other-window -1))
(global-set-key (kbd "<C-prior>") 'vscode-previous-window)    ;; Ctrl-PageUp
(global-set-key (kbd "<C-next>") 'other-window)    ;; Ctrl-PageDown

(defun vscode-insert-line-above()
  (interactive)
  (previous-line 1)
  (move-end-of-line 1)
  (newline-and-indent)
)
(global-set-key (kbd "C-S-<return>") 'vscode-insert-line-above)

(defun vscode-insert-line-below()
  (interactive)
  (move-end-of-line 1)
  (newline-and-indent)
)
(define-key cua-global-keymap (kbd "C-<return>") 'vscode-insert-line-below)

(defun vscode-duplicate-line-up()
  (interactive)
  (kill-whole-line)
  (yank)
  (yank)
  (previous-line 2)
  (move-end-of-line 1)
)
(global-set-key (kbd "M-S-<up>") 'vscode-duplicate-line-up)

(defun vscode-duplicate-line-down()
  (interactive)
  (kill-whole-line)
  (yank)
  (yank)
  (previous-line 1)
  (move-end-of-line 1)
)
(global-set-key (kbd "M-S-<down>") 'vscode-duplicate-line-down)

(defun vscode-move-line-up()
  (interactive)
  (transpose-lines 1)
  (forward-line -2))
(global-set-key (kbd "M-<up>") 'vscode-move-line-up)

(defun vscode-move-line-down()
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1))
(global-set-key (kbd "M-<down>") 'vscode-move-line-down)

(progn    ;; Teclas que serão precedidas pelo prefixo Ctrl-k
  (define-prefix-command 'vscode-ctrl-k-key-map)
  (define-key vscode-ctrl-k-key-map (kbd "C-c") 'comment-line)
  (define-key vscode-ctrl-k-key-map (kbd "C-u") 'comment-line)
  )
(global-set-key (kbd "C-k") vscode-ctrl-k-key-map)  ;; Define o prefixo Ctrl-k

;; ------------------------------------------------------------

